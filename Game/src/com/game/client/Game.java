package com.game.client;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.*;

import org.lwjgl.glfw.GLFWVidMode;

public class Game implements Runnable
{
	
	private int WIDTH = 840;
	private int HEIGTH = 720;
	private Thread thread;
	private boolean Running = false;
	private long window;
	
	/**
	 * Starts The Game.
	 */
	public void Start() 
	{
		Running = true;
		thread = new Thread(this, "Game");
		thread.start();
	}
	
	/**
	 * Creates The Window.
	 */
	private void Display()
	{	
		if (glfwInit() != false)
		{
			return;
		}
	
		glfwWindowHint(GLFW_RESIZABLE, 1);
		window = glfwCreateWindow(WIDTH, HEIGTH, "Game", NULL, NULL);
		
		if(window == NULL)
		{
			return;
		}
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		glfwSetWindowPos(window, (GLFWVidMode.WIDTH - WIDTH) / 2, (GLFWVidMode.HEIGHT - HEIGTH) / 2);
		glfwMakeContextCurrent(window);
		glfwShowWindow(window);
	}
	
	/**
	 * Game Loop.
	 */
	public void run() 
	{
		Display();
		while(Running)
		{
			update();
			render();
			if (glfwWindowShouldClose(window) == true)
			{
				Running = false;
			}
		}
	}

	public void update() 
	{
		glfwPollEvents();
	}
	
	public void render()
	{
		glfwSwapBuffers(window);
	}
}
